import csv
import numpy as np
import matplotlib.pyplot as plt
import sys

print sys.argv

data = open(sys.argv[1] if len(sys.argv) > 1 else "sheet.csv")

reader = csv.reader(data)

# Page values of each index
pages = []
# Time values of each index
times = []
# stdevs of each pages value (0 - 6)
stdevs = []
# averages of each pages value (0 - 6)
avgs = []

# Skip the first value
rowiter = iter(reader)
next(rowiter)

for row in rowiter:
    # Get number of actual y values and append x value for each y value
    size = len(row) - 1
    for x in range(0, size):
        pages.append(row[0])
    # Remove x value of row
    del row[0]
    # Add all y values
    times.extend(row)
    # Get standard new values for analysis
    newvals = np.array(row).astype(np.float)
    # Get stdev
    stdevs.append(np.std(newvals))
    avgs.append(np.mean(newvals))

print stdevs, avgs
# Scatter
pages = np.array(map(float, pages))
times = np.array(map(float, times))

plt.scatter(pages, times, alpha=0.2, c='b', marker='o')
# Error bars
rangelimiter = int(max(pages) + 1)
plt.errorbar(range(rangelimiter), avgs, yerr=stdevs, fmt='ro', ecolor='k')
for i in range(rangelimiter):
    stdevval = "stdev: " + str(stdevs[i])[:6]
    plt.annotate(stdevval, xy=(i + .05, avgs[i] + stdevs[i]))
    avgval = "avg: " + str(avgs[i])[:6]
    plt.annotate(avgval, xy=(i + .05, avgs[i]))


# LinRegression
coefficients = np.polyfit(pages, times, 1)
polynomial = np.poly1d(coefficients)

xmin = min(pages) - .5
xmax = max(pages) + .5
n = 1000
x_fit = np.linspace(xmin, xmax, n)
y_fit = polynomial(x_fit)

plt.plot(x_fit, y_fit, '-g')

# Graph labeling and limiting
plt.ylim([min(times) - .1, max(times) + .1])
plt.xlim([xmin, xmax])

plt.xlabel("Number of half folds")
plt.ylabel("Time to floor (sec)")

# Show and finish up
plt.show()

data.close()
